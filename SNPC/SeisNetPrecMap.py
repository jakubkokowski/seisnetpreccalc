#  Seismic Network Precision Calculator (SeisNetPrecCalc)
#
#  Mapping module
#
#  Copyright (C) 2022  Jakub Kokowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import os

import matplotlib.pyplot as plt
import numpy as np
from scipy import ndimage

from SNPC_config_loader import config
from SeisNetPrecCalc import SNPC
from SeisNetPrecCalc import create_dir_if_not_exist


def prepare_stations() -> [list, list, list]:
    """Function read values from stations file.

    :return: lists of stations x and y coordinates and their names from stations file
    """

    # Read stations from file
    stations = SNPC.read_stations()
    stations_names = []
    stations_x = []
    stations_y = []

    for station_name, station_info in stations.items():
        stations_x.append(station_info["Station_X"])
        stations_y.append(station_info["Station_Y"])
        stations_names.append(station_name)

    return stations_x, stations_y, stations_names


class _SeisNetPrecMap:
    def __init__(self):
        self.config = config

    def check_max_error(self,errors: list) -> float:
        """Function search for maximum location errors.

        :param errors:
        :return:
        """
        return max([x for x in errors[:, 5] if x < self.config.Error_value])

    def prepare_xyz_input(self, errors: np.ndarray) -> [np.array, np.array, np.array]:
        """Function prepares x, y, z numpy arrays for visualization.

        :param errors: numpy ndarray with location errors calculated by SNPC
        :return: x,y,z numpy array's
        """

        hor_error = errors[:, 5]
        # mask area with smaller number of stations which detected event than 4 (value required for proper location)
        mask = [1 if x < 4 else 0 for x in errors[:, 6]]
        x_range = np.arange(self.config.X_Min, self.config.X_Max, self.config.Step)
        y_range = np.arange(self.config.Y_Min, self.config.Y_Max, self.config.Step)
        y, x = np.meshgrid(y_range, x_range)
        z = hor_error.reshape(x.shape)
        z = ndimage.gaussian_filter(z, sigma=self.config.FilterCoeff, order=0)  # 0.8
        z = np.ma.array(z, mask=mask)
        return x, y, z

    def visualize_error_map(self, filename: str):
        """Function creates matplotlib.pyplot plot basing on SNPC results.

        :param filename: str with SNPC location errors results
        :return:
        """

        # Read errors from file
        errors = np.load(os.path.join(self.config.PrecisionMapsDirPath, filename))
        x, y, z = self.prepare_xyz_input(errors)
        stations_x, stations_y, stations_names = prepare_stations()

        # Plot
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.set_xlabel('X [m]', fontsize=12)
        ax.set_ylabel('Y [m]', fontsize=12)
        cs = ax.contourf(x, y, z, alpha=0.8, cmap='Reds',  levels=self.config.ContourLevels)
        for i in range(len(stations_x)):
            ax.scatter(stations_x[i], stations_y[i], c='white', edgecolor='black', marker='v', s=100)
            ax.annotate(stations_names[i], (stations_x[i], stations_y[i]+70))
        CB = fig.colorbar(cs, shrink=0.8, extend='both')
        CB.set_label('Epicentral error [m]', fontsize=12)
        if self.config.SaveMap:
            create_dir_if_not_exist(self.config.PrecisionMapsImagesDirPath)
            plt.savefig(os.path.join(self.config.PrecisionMapsImagesDirPath, filename[:-4] + '.png'), dpi=300)
        plt.show()


SNPM = _SeisNetPrecMap()

if __name__ == '__main__':
    SNPM.visualize_error_map('21022022095649_0_3500_0_3500_100_600.npy')
