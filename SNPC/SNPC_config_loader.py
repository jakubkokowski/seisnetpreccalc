import os
import SNPCconfig_file as SNPCconf


class _SNPCConfigLoader:
    def __init__(self):
        # print('Loading config')
        self.Delta = 40  # distance between two points in numerical calculation of time derivatives.
        self.Error_value = 1000000  # value returned in all covariance matrix elements when event is not detected
        # load all parameters from configuration file
        self.Ranges = SNPCconf.Ranges
        self.InputFilesDirPath = SNPCconf.FilesDirPath
        self.StationsCoordFile = os.path.join(self.InputFilesDirPath, SNPCconf.StationsCoordinatesPath)
        self.StationsRangesCoordFile = os.path.join(self.InputFilesDirPath, SNPCconf.StationsRangesPath)
        self.VelocityModelFile = os.path.join(self.InputFilesDirPath, SNPCconf.VelocityModelFile)
        self.PrecisionMapsDirPath = os.path.join(self.InputFilesDirPath, SNPCconf.PrecisionMapsDirPath)
        self.VelocityModelDirPath = os.path.join(self.InputFilesDirPath, SNPCconf.VelocityModelDirPath)
        self.PrecisionMapsImagesDirPath = os.path.join(self.InputFilesDirPath, SNPCconf.PrecisionMapsImagesDirPath)
        self.GridXlength = int(SNPCconf.VelModPar["X_Dimension"])
        self.GridZlength = int(SNPCconf.VelModPar["Z_Dimension"])
        self.GridZStart = int(SNPCconf.VelModPar["Z_Start"])
        self.GridSize = int(SNPCconf.VelModPar["Grid_Size"])
        self.PWaveArrivalTimeParameters = SNPCconf.PWaveArrivalTimeError
        self.SWaveArrivalTimeParameters = SNPCconf.SWaveArrivalTimeError
        self.Vp_vs = int(SNPCconf.VelModPar["Vp/Vs_ratio"])
        self.Conf_lvl = float(SNPCconf.ConfLvlPar["Confidence_Level"])
        self.Dimension = int(SNPCconf.ConfLvlPar["Dimension"])
        self.X_Min = int(SNPCconf.PrecMapPar["X_Min"])
        self.X_Max = int(SNPCconf.PrecMapPar["X_Max"])
        self.Y_Min = int(SNPCconf.PrecMapPar["Y_Min"])
        self.Y_Max = int(SNPCconf.PrecMapPar["Y_Max"])
        self.Step = int(SNPCconf.PrecMapPar["Step"])
        self.Source_Depth = int(SNPCconf.PrecMapPar["Source_Depth"])
        self.FilterCoeff = float(SNPCconf.ErrorMap["Filter_Coeff"])
        self.SaveMap = float(SNPCconf.ErrorMap["Save_Map"])
        self.ContourLevels = SNPCconf.ErrorMap["Contour_Levels"]
        # Velocity grid parameters
        # Start of grid: values of xstart and ystart are bigger than 0 for numerical purposes
        self.xstart = 10
        self.ystart = 10
        # Grid length
        self.nx = int(self.GridXlength / self.GridSize) + 1 + self.xstart
        self.ny = int(self.GridZlength / self.GridSize) + 1 + self.ystart

    def __getattr__(self, name):
        return 'Config file does not contain `{}` attribute.'.format(str(name))


config = _SNPCConfigLoader()

