#  Seismic Network Precision Calculator (SeisNetPrecCalc)
#
#  Main File
#
#  Copyright (C) 2022  Jakub Kokowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from datetime import datetime
import os
import warnings

import numpy as np
from scipy import stats

from SNPC_config_loader import config


def create_dir_if_not_exist(path: str):
    """ Creates directory if does not exist before.

    :param path: string path to the directory
    :return:
    """

    if not os.path.isdir(path):
        os.mkdir(path)
        print("created folder : ", path)


def distance_depth_calculator(stations_x: np.ndarray, stations_y: np.ndarray, stations_z: np.ndarray,
                              event_xyz: [float, float, float]) -> [list, list]:
    """Function calculate horizontal and vertical distances between stations and source.

    :param stations_x: list with x coordinates of stations
    :param stations_y: list with y coordinates of stations
    :param stations_z: list with z coordinates of stations
    :param event_xyz: seismic event coordinates [x: float, y: float, z: float]
    :return:  lists of horizontal and vertical distances.
    """

    horizontal_distances = []
    vertical_distances = []
    for i in range(len(stations_x)):
        horizontal_distances.append(np.sqrt(np.power((event_xyz[0] - stations_x[i]), 2) +
                                            np.power((event_xyz[1] - stations_y[i]), 2)))
        vertical_distances.append(event_xyz[2] - stations_z[i])
    horizontal_distances = np.array(horizontal_distances)
    vertical_distances = np.array(vertical_distances)
    return horizontal_distances, vertical_distances


def arrival_time_error(horizontal_distances: np.ndarray, *args: list) -> np.ndarray:
    """Function calculate arrival time errors.

     Function is basing on horizontal source-receiver distances and polynomial approximation of errors
     with *args arguments (declared in config file).

    :param horizontal_distances:
    :param args: arguments of polynomial declared in config file
    :return: numpy array of arrival time errors for seismic stations
    """

    errors = np.zeros(len(horizontal_distances))
    for i, x in enumerate(args):
        errors += x * horizontal_distances ** i
    return errors


def chi_value(confidence_lvl=68.3, dimension=2) -> float:
    """Function return chi-square value for given confidence level (default 68%) and given dimension (default 2 (2D- horizontal))

    :param confidence_lvl:
    :param dimension:
    :return: chi value float value
    """

    return stats.chi2.ppf(confidence_lvl / 100, dimension)


class _SeisNetPrecCalc:
    def __init__(self):
        self.config = config

    def epicentral_error(self, cov_matrix: np.ndarray, chi=1.0) -> float:
        """Function return epicentral location error as mean axis of error ellipse for given chi value.

        :param cov_matrix: covariance matrix
        :param chi: default chi_value=1 which give 39% confidence level- for 2 dimensions.
        :return: epicentral location error [float]
        """

        if cov_matrix[0][0] != self.config.Error_value:
            return np.power(abs(cov_matrix[0, 0] * cov_matrix[1, 1] - cov_matrix[0, 1] ** 2), 0.25) * np.sqrt(chi)
        else:
            return self.config.Error_value

    def load_velocity_model_multiple(self, distances: np.ndarray, depths: np.ndarray) -> list:
        """Function return list of arrival times for multiple stations.

        :param distances: source-receiver horizontal distances in meters
        :param depths: source-receiver vertical distances in meters
        :return: list of arrival times for multiple stations
        """

        t = np.load(self.config.VelocityModelDirPath + '/TimeMatrix.npy')
        tt = []
        for i in range(len(distances)):
            index_x = int(distances[i] / self.config.GridSize)
            delta_x = distances[i] % self.config.GridSize
            wx0 = (self.config.GridSize - delta_x) / self.config.GridSize
            wx1 = delta_x / self.config.GridSize
            index_h = int(
                depths[i] / self.config.GridSize)  # tu można uwzględnić wysokość stacji - póki co to nie dziala
            delta_y = depths[i] % self.config.GridSize  # tu można uwzględnić wysokość stacji - póki co to nie dziala
            wy0 = (self.config.GridSize - delta_y) / self.config.GridSize
            wy1 = delta_y / self.config.GridSize
            try:
                ty0 = t[index_x, index_h] * wy0 + t[index_x, index_h + 1] * wy1
                ty1 = t[index_x + 1, index_h] * wy0 + t[index_x + 1, index_h + 1] * wy1
                tt.append(ty0 * wx0 + ty1 * wx1)
            except IndexError:
                print("Problem with reading velocity model file: the index exceeds the size of the time table")
        return tt

    def read_stations(self) -> dict:
        """Function load stations from stations file.

        :return: dict with stations information
        """

        stations = dict()
        try:
            stations_file = open(self.config.StationsCoordFile, "r")
            # Use print to print the line else will remain in buffer and replaced by next statement
            for line in stations_file:
                if line[0] != "#":
                    values = line.split(';')
                    station = {
                        "Station_X": int(values[1]),
                        "Station_Y": int(values[2]),
                        "Station_Z": int(values[3])
                    }
                    stations[values[0]] = station

            stations_file.close()
        except IOError:
            print("File not found or path is incorrect")
        if not stations:
            print("Stations File does not contain stations")
        return stations

    def read_stations_ranges(self) -> dict:
        """Function load stations ranges from stations range file.

        :return: dict with stations ranges
        """

        stations = dict()
        try:
            ranges_file = open(self.config.StationsRangesCoordFile, "r")
            # Use print to print the line else will remain in buffer and replaced by next statement
            for line in ranges_file:
                if line[0] != "#":
                    values = line.split(';')
                    station = {
                        "Range": int(values[1]),
                    }
                    stations[values[0]] = station

            ranges_file.close()
        except IOError:
            print("File not found or path is incorrect")
        if not stations:
            print("Stations Ranges File does not contain stations")
        return stations

    def time_function_derivatives(self, stations_x: np.ndarray, stations_y: np.ndarray, stations_z: np.ndarray,
                                  event_xyz: [float, float, float], delta: int) -> [np.ndarray, np.ndarray,
                                                                                    np.ndarray]:
        """Function calculates time function derivatives.

        :param stations_x: numpy.ndarray of stations x coordinates
        :param stations_y: numpy.ndarray of stations y coordinates
        :param stations_z: numpy.ndarray of stations z coordinates
        :param event_xyz: [x: float, y: float, z: float]
        :param delta: int, distance for numerical purposes of derivatives calculation.
        :return: [x derivatives: numpy.ndarray, y derivatives: numpy.ndarray, z derivatives:numpy.ndarray]
        """

        horizontal_distances, vertical_distances = distance_depth_calculator(stations_x, stations_y,
                                                                             stations_z, event_xyz)
        t1 = np.array(self.load_velocity_model_multiple(horizontal_distances + delta / 2, vertical_distances))
        t2 = np.array(self.load_velocity_model_multiple(horizontal_distances - delta / 2, vertical_distances))

        # l = np.sqrt(np.power((x - stations_x), 2) + np.power((y - stations_y), 2))
        warnings.filterwarnings("error")
        try:
            x_der = (t1 - t2) * (event_xyz[0] - stations_x) / (delta * horizontal_distances)
            y_der = (t1 - t2) * (event_xyz[1] - stations_y) / (delta * horizontal_distances)
        except RuntimeWarning:
            x_der = (t1 - t2) * (event_xyz[0] - stations_x) / (delta * (horizontal_distances + 1.0))
            y_der = (t1 - t2) * (event_xyz[1] - stations_y) / (delta * (horizontal_distances + 1.0))
        z_der = (t1 - t2) / delta
        warnings.filterwarnings("default")
        return x_der, y_der, z_der

    def unpack_station(self, stations_x: list, stations_y: list, stations_z: list, station_info: dict) -> \
            [list, list, list]:
        """Function add station info from dictionary to lists with stations coordinates.

        :param stations_x: list of stations x coordinates
        :param stations_y: list of stations y coordinates
        :param stations_z: list of stations z coordinates
        :param station_info:
        :return: [stations_x: list, stations_y: list, stations_z: list]
        """
        stations_x.append(station_info["Station_X"])
        stations_y.append(station_info["Station_Y"])
        stations_z.append(station_info["Station_Z"])

        return stations_x, stations_y, stations_z

    def check_stations_ranges(self, event_xyz:[float, float, float]) -> [np.ndarray, np.ndarray, np.ndarray]:
        """Search for stations within range.

        :param event_xyz: [x: float, y: float, z: float]
        :return: Lists of coordinates of stations within range [stations_x: numpy.ndarray, stations_y:
        numpy.ndarray, stations_z: numpy.ndarray]
        """

        stations = self.read_stations()

        # Choosing stations within range
        stations_x = []
        stations_y = []
        stations_z = []
        if self.config.Ranges:
            ranges = self.read_stations_ranges()
            for station_name, station_info in stations.items():
                distance, ver_dist = distance_depth_calculator(station_info["Station_X"], station_info["Station_Y"],
                                                               station_info["Station_Z"], event_xyz)
                if distance[0] <= int(ranges[station_name]['Range']):
                    stations_x, stations_y, stations_z = self.unpack_station(stations_x, stations_y, stations_z,
                                                                             station_info)
                stations[station_name].update(ranges[station_name])
        else:
            for station_name, station_info in stations.items():
                stations_x, stations_y, stations_z = self.unpack_station(stations_x, stations_y, stations_z,
                                                                         station_info)

        return np.array(stations_x), np.array(stations_y), np.array(stations_z)

    def precision(self, event_xyz: [float, float, float]) -> [np.ndarray, int]:
        """Calculate values for precision maps.

        :param event_xyz: seismic events coordinates [x: float, y: float, z: float]
        :return: [cov_matrix: numpy.ndarray, number_of_stations: int]
        """

        # check stations ranges
        stations_x, stations_y, stations_z = self.check_stations_ranges(event_xyz)
        # situation when event is not detected
        number_of_stations = len(stations_x)
        if number_of_stations < 4:
            return self.config.Error_value * np.ones((4, 4)), number_of_stations
        else:
            # x,y,z derivatives for 1D velocity model: P wave 
            x_der_p, y_der_p, z_der_p = self.time_function_derivatives(stations_x, stations_y, stations_z, event_xyz,
                                                                       self.config.Delta)
            # x,y,z derivatives for 1D velocity model: S wave 
            x_der_s = x_der_p * self.config.Vp_vs
            y_der_s = y_der_p * self.config.Vp_vs
            z_der_s = z_der_p * self.config.Vp_vs

            x_der = np.append(x_der_p, x_der_s)
            y_der = np.append(y_der_p, y_der_s)
            z_der = np.append(z_der_p, z_der_s)
            tcol = np.ones(len(x_der))

            g = np.append(x_der, (y_der, z_der, tcol))
            g = np.reshape(g, (4, (len(x_der)))).T
            gg = np.linalg.pinv(g)

            horizontal_distance = distance_depth_calculator(stations_x, stations_y, stations_z, event_xyz)[0]
            p_polynomial_terms = self.config.PWaveArrivalTimeParameters.values()
            p_errors = arrival_time_error(horizontal_distance, *p_polynomial_terms)
            s_polynomial_terms = self.config.SWaveArrivalTimeParameters.values()
            s_errors = arrival_time_error(horizontal_distance, *s_polynomial_terms)
            print(s_errors)
            errors = np.append(p_errors, s_errors)
            cd = np.diag(errors)
            cov_matrix = np.matmul(np.matmul(gg, cd), gg.T)
            return cov_matrix, number_of_stations

    def precision_map_calculator(self) -> str:
        """Function calculate values for precision maps and save them in a file.

        :return: filename str
        """

        now = datetime.now()
        dt_string = now.strftime("%d%m%Y%H%M%S")
        file_name = dt_string + '_' + str(self.config.X_Min) + '_' + str(self.config.X_Max) + '_' \
                    + str(self.config.Y_Min) + '_' + str(self.config.Y_Max) + '_' + str(self.config.Step) + '_' \
                    + str(self.config.Source_Depth) + '.npy'
        x_range = np.arange(self.config.X_Min, self.config.X_Max, self.config.Step)
        y_range = np.arange(self.config.Y_Min, self.config.Y_Max, self.config.Step)
        i = 0
        results = np.empty((len(x_range) * len(y_range), 7))
        for event_X in x_range:
            for event_Y in y_range:
                event_xyz = np.array([event_X, event_Y, self.config.Source_Depth])
                cov_matrix, stations = SNPC.precision(event_xyz)
                horizontal_error = self.epicentral_error(cov_matrix, chi_value(confidence_lvl=self.config.Conf_lvl,
                                                                               dimension=self.config.Dimension))
                print("Finished for:" + str(event_xyz[:2]) + '| Uncertainty for ' + str(self.config.Dimension) +
                      ' dims and conf level ' + str(self.config.Conf_lvl) + ': ' + str(round(horizontal_error)) + ' [m]')
                results[i] = [event_X, event_Y, np.sqrt(cov_matrix[0][0]), np.sqrt(cov_matrix[1][1]),
                              np.sqrt(cov_matrix[2][2]), horizontal_error, stations]
                i += 1
        create_dir_if_not_exist(self.config.PrecisionMapsDirPath)
        np.save(os.path.join(self.config.PrecisionMapsDirPath, file_name), results)
        return file_name


SNPC = _SeisNetPrecCalc()


if __name__ == '__main__':
    os.chdir('..')
    SNPC.precision_map_calculator()
