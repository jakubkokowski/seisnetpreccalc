# Seismic Network Precision Calculator (SeisNetPrecCalc)
# Module for calculating velocity model
#
# A program for calculating the velocity model using the Fast Sweeping Method
# Source: https://tcs.ah-epos.eu/eprints/1592/1/TRCloc_manual-v4.2.pdf
# TRMLOC Reference manual version 4.2;  Wojciech Dębski (2015)
#
#  Copyright (C) 2022  Jakub Kokowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np

from SeisNetPrecCalc import create_dir_if_not_exist
from SNPC_config_loader import config


class _SeisNetPrecVel:
    def __init__(self):
        self.config = config

    def read_velocity_model_file(self) -> [list, list]:
        """ Function read velocity 1D model file.

        :return: Function returns list of depths to top of layers and list of velocities
        """

        v_model_h = []
        v_model_v = []
        try:
            velocity_file = open(self.config.VelocityModelFile, "r")
            # Use print to print the line else will remain in buffer and replaced by next statement
            for line in velocity_file:
                if line[0] != "#":
                    values = line.split(';')
                    v_model_v.append(int(values[1]))
                    v_model_h.append(int(values[0]))
            velocity_file.close()
        except IOError:
            print("File not found or path is incorrect")
        if v_model_v is False or v_model_h is False:
            print("Model is empty")

        return v_model_h, v_model_v

    def alternating_sweeping(self, T: np.ndarray, S: np.ndarray, x_min: int, x_max: int, y_min: int, y_max: int,
                             x_step: int, y_step: int) -> np.ndarray:
        """ Function performs alternating sweeping which is part of the Fast Sweeping Method

        :param T: 2D numpy array - time grid
        :param S: 2D numpy array - slowness grid
        Grid size parameters:
        :param x_min: int - minimum value of x
        :param x_max: int - maximum value of x
        :param y_min: int - minimum value of u
        :param y_max: int - maximum value of y
        :param x_step: int - step in x direction
        :param y_step: int - step in y direction
        :return: recalculated time grid 2D numpy array
        """

        for i in range(x_min, x_max, x_step):
            for j in range(y_min, y_max, y_step):
                if T[i, j] != 0:
                    Txmin = min(T[i - 1, j], T[i + 1, j])
                    Tymin = min(T[i, j - 1], T[i, j + 1])
                    condition = S[i, j] * self.config.GridSize
                    if abs(Txmin - Tymin) >= condition:
                        Tij_new = min(Txmin, Tymin) + condition
                    else:
                        Tij_new = (Txmin + Tymin + np.sqrt(
                            2 * S[i, j] ** 2 * self.config.GridSize ** 2 - (Txmin - Tymin) ** 2)) / 2
                    T[i, j] = min(T[i, j], Tij_new)
        return T

    def load_velocity_model_to_eikonal(self, V: np.ndarray) -> np.ndarray:
        """Function fill velocity grid for the Fast Sweeping Method with values from velocity model file.

        :param V: velocity grid filled with initial values
        :return: velocity grid with values from velocity model file
        """

        # fill velocity matrix with values from the velocity model
        v_model_h, v_model_v = self.read_velocity_model_file()
        index_start = 0
        stop_flag = False
        for index, depth in enumerate(v_model_h):
            if not stop_flag:
                index_h = int(depth / self.config.GridSize) + 1 + self.config.ystart
                if index_h >= self.config.ny:
                    stop_flag = True
                    index_h = self.config.ny
                for i in range(self.config.nx):
                    for j in range(index_start, index_h):
                        V[i, j] = v_model_v[index]
                index_start = index_h
        return V

    def initiate_time_matrix(self) -> [np.ndarray, np.ndarray]:
        """Function creates time and slowness grids using values from velocity model file.

        :return: time and slowness grids
        """

        # velocity grid
        V = np.ones([self.config.nx + 1, self.config.ny + 1])  # velocity matrix
        # time grid
        T = V * 100000  # time matrix (initially filled with big values)
        # fill velocity grid with values from the velocity model
        V = self.load_velocity_model_to_eikonal(V)
        # slowness grid
        S = 1 / V  # slowness [s/m] matrix
        # fill time in source with 0 values
        T[self.config.xstart, self.config.ystart] = 0
        T[self.config.xstart - 1, self.config.ystart] = 0
        T[self.config.xstart - 1, self.config.ystart - 1] = 0
        T[self.config.xstart, self.config.ystart - 1] = 0

        return T, S

    def eikonal_solver_reciprocal(self) -> np.ndarray:
        """Function calculate travel times using the Fast Sweeping Method for given velocity 1D model file.

        :return: 2D time array
        """

        T, S = self.initiate_time_matrix()
        # Alternating sweeping
        XXX = 2
        while XXX > 0:
            # print(XXX)
            T = self.alternating_sweeping(T, S, x_min=1, x_max=self.config.nx - 1, x_step=1, y_min=1,
                                          y_max=self.config.ny - 1, y_step=1)
            T = self.alternating_sweeping(T, S, x_min=1, x_max=self.config.nx - 1, x_step=1, y_min=self.config.ny - 1,
                                          y_max=1, y_step=-1)
            T = self.alternating_sweeping(T, S, x_min=self.config.nx - 1, x_max=1, x_step=-1, y_min=1,
                                          y_max=self.config.ny - 1, y_step=1)
            T = self.alternating_sweeping(T, S, x_min=self.config.nx - 1, x_max=1, x_step=-1, y_min=self.config.ny - 1,
                                          y_max=1, y_step=-1)
            XXX -= 1
        return T[self.config.xstart:, self.config.ystart:]

    def model_creator_reciprocal(self):
        """Function save travel time tables calculated by eikonal_solver_reciprocal function.

        :return:
        """

        create_dir_if_not_exist(self.config.VelocityModelDirPath)
        time_matrix = self.eikonal_solver_reciprocal()
        np.save(self.config.VelocityModelDirPath + '/' + "TimeMatrix", time_matrix)

    def load_velocity_model(self, station_xyz: [float, float, float], event_xyz: [float, float, float]):
        """Function return time between source and station and horizontal distance between them.

        :param station_xyz: x,y,z coordinates in meters [float, float, float]
        :param event_xyz: x,y,z coordinates in meters  [float, float, float]
        :return:
        """

        T = np.load(self.config.VelocityModelDirPath + '/TimeMatrix.npy')
        S = np.sqrt(np.power((event_xyz[0] - station_xyz[0]), 2) + np.power((event_xyz[1] - station_xyz[1]), 2))
        index_x = int(S / self.config.GridSize)
        delta_x = S % self.config.GridSize
        wx0 = (self.config.GridSize - delta_x) / self.config.GridSize
        wx1 = delta_x / self.config.GridSize
        index_h = int(event_xyz[2] / self.config.GridSize)   # tu można uwzględnić wysokość stacji - póki co to nie dziala
        delta_y = event_xyz[2] % self.config.GridSize      # tu można uwzględnić wysokość stacji - póki co to nie dziala
        wy0 = (self.config.GridSize - delta_y) / self.config.GridSize
        wy1 = delta_y / self.config.GridSize
        try:
            ty0 = T[index_x,index_h]*wy0+T[index_x,index_h+1]*wy1
            ty1 = T[index_x+1,index_h]*wy0+T[index_x+1,index_h+1]*wy1
            t = ty0*wx0 + ty1*wx1
            return np.round(t, 2), np.round(S, 2)
        except IndexError:
            print("Problem with reading velocity model file: the index exceeds the size of the time table")


SNPV = _SeisNetPrecVel()

if __name__ == '__main__':
    station_xyz_example = [0, 0, 0]
    event_xyz_example = [0, 500, 0]
    SNPV.model_creator_reciprocal()
    print(SNPV.load_velocity_model(station_xyz_example, event_xyz_example))
