# Seismic Network Precision Calculator (SeisNetPrecCalc)
# Author: Jakub Kokowski (2022)
#
# Configuration File
#
#

# Paths to files and directories
# General path to input files
FilesDirPath = "Files"
# Path to txt seismic stations coordinates file
StationsCoordinatesPath = "stations_coordinates.txt"
# Path to txt stations ranges coordinates file
StationsRangesPath = "ranges_file.txt"
# Path to velocity model file
VelocityModelFile = "velocity_model.txt"
# Path to directory with velocity model tables
VelocityModelDirPath = "VelocityModel_time_matrix"
# Path to the map data files directory
PrecisionMapsDirPath = "Precision_Maps_Files"
# Path to the map images files directory
PrecisionMapsImagesDirPath = "Precision_Maps_Images"

# True if you want to use stations ranges file, False if ranges are infinite
Ranges = False

# Parameters of 1D velocity model: have to be the same for precision calculation and for created velocity model
VelModPar = {
    # Grid length [meters]- integer
    "X_Dimension": 5000,
    # Grid height [meters] - integer
    "Z_Dimension": 900,
    # Grid Z start [meters] - integer
    "Z_Start": 0,  # doesn't work yet
    # Grid size [meters]- integer
    "Grid_Size": 10,
    # Vp/Vs ratio - float
    "Vp/Vs_ratio": 1.73,
}

# Parameters for precision map calculation
PrecMapPar = {
    # Grid parameters: integers [meters]
    "X_Min": 0,
    "X_Max": 3500,
    "Y_Min": 0,
    "Y_Max": 3500,
    "Step": 100,
    "Source_Depth": 600,  # depth of seismic event source for which the map is calculated
}
# Confidence level parameters
ConfLvlPar = {
    "Confidence_Level": 68.3,  # Confidence level in % (float)
    "Dimension": 2,  # Value of dimension for chi-square calculation (1,2,3,4)
}

# Polynomial terms: only order (not names) are important.
PWaveArrivalTimeError = {
    "a": 3.25e-02,  # a constant term
    "b": 3.57e-06,  # term at x
}

SWaveArrivalTimeError = {
    "a": 6.23e-02,  # a constant term
    "b": -1.24e-05,  # term at x
    "c": 2.33e-09,  # term at x2
}

# Example of constant errors
# Polynomial terms: only order (not names) are important.
# PWaveArrivalTimeError = {
#     "a": 0.01,  # a constant term
# }

# SWaveArrivalTimeError = {
#     "a": 0.03,  # a constant term
# }

# Plot parameters
ErrorMap = {
    # Levels
    "Contour_Levels": range(0, 2400, 300),  # list of levels ex. [0, 200, 1000, 2000] or python range
    # Filter Coefficient: when bigger, the map is more smoothed
    "Filter_Coeff": 0.8,  # float
    # To save map to the file: True
    "Save_Map": True,  # Boolean
}


