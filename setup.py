from setuptools import setup

setup(
    name='SeismicNetworkPrecisionCalculator',
    version='0.0.1',
    packages=['SNPC'],
    url='https://bitbucket.org/jakubkokowski/automaticlocations/src',
    license='GNU GENERAL PUBLIC LICENSE',
    author='Jakub Kokowski',
    author_email='jakubkokowski@gmail.com',
    description='Software for calculating theoretical location precisions for seismic networks'
)
