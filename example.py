from SNPC import SNPV
from SNPC import SNPC
from SNPC import SNPM

# Creates time table from velocity model file
SNPV.model_creator_reciprocal()
# Calculates precision values
file = SNPC.precision_map_calculator()
# Creates a map of errors
SNPM.visualize_error_map(file)  # file: name of file in directory containing results of SNPC
