# Seismic Network Precision Calculator #

Program designed for calculating theoretical precision maps for seismic networks.

![Epicentral errors](./Assets/epicentral_errors.png)

 Copyright (C) 2022  Jakub Kokowski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

***
## Program modules ##

- SNPCVel 
> Module used for calculating travel time tables basing on the Fast Sweeping Method 
> Source: TRMLOC Reference manual version 4.2;  Wojciech Dębski (2015) (https://tcs.ah-epos.eu/eprints/1592/1/TRCloc_manual-v4.2.pdf)
- SNPCCalc
> Main program. It is used for calculating precision values for given seismic network
- SNPCMap
> Module used for mapping the results from SNPCCalc module

***
## Getting started ##

Clone or download the project from BitBucket, if needed uncompress the archive.

### Configuration files ###
- **SNPCconfig_file.py** contain all paths and other configuration parameters. 
You can edit this file for your own purposes.
- **stations_coordinates.txt** contain coordinates of seismic stations
- **ranges_file.txt** contain information about distance ranges of seismic stations. Ranges may be 
obtained by analysis of probability plots (containing information about percentage of detected events for different
distances and magnitudes). It is optional: if stations 
ranges are not used choose *Ranges = False* in **SNPCconfig_file.py**) 
- **velocity_model.txt** contain information about 1D velocity model.  

### Running examples ###
- **example.py** file contain example of full procedure needed to obtain a precision map

***
## Documentation ##

[Documentation](https://bitbucket.org/jakubkokowski/seisnetpreccalc/wiki/Home)

***
## References ##
- Kokowski J., Rudziński Ł., (unpublished): *Estimation of earthquake location errors distribution for local 
seismic network*

***
## Author ##

Jakub Kokowski (2022) 

Department of Seismology, Institute of Geophysics, Polish Academy of Sciences

email: *jkokowski@igf.edu.pl*
